package br.com.httpfluidobjects.vouper.Outros;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.httpfluidobjects.vouper.R;
import br.com.httpfluidobjects.vouper.Types.Categoria;

public class AdapterPermicoes extends ArrayAdapter<Categoria> {
   // private final String MY_DEBUG_TAG = "CategoriaAdapter";
    private ArrayList<Categoria> items;
    private ArrayList<Categoria> itemsAll;
    private ArrayList<Categoria> suggestions;
    private int viewResourceId;

    public AdapterPermicoes(Context context, ArrayList<Categoria> items) {
        super(context, 0, items);
        this.items = items;
        this.itemsAll = (ArrayList<Categoria>) items.clone();
        this.suggestions = new ArrayList<Categoria>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_categoria, null);
        }
        final CategoriaHolder holder = new CategoriaHolder();
        holder.categoria = items.get(position);
        holder.name = (TextView) v.findViewById(R.id.categoria);
        holder.check = v.findViewById(R.id.check);
        holder.check.setTag(holder.categoria);
        holder.name.setText(holder.categoria.getTitle());
        holder.linha = v.findViewById(R.id.line);
        holder.linha.setTag(holder.lineSelected);

        selectedLine(holder);
        v.setTag(holder);
        return v;
    }

    private void selectedLine(final CategoriaHolder holder){
        holder.linha.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!holder.lineSelected){
                    holder.lineSelected = true;
                    v.setBackgroundColor(v.getResources().getColor(R.color.vouperFundo));
                    TextView tv = v.findViewById(R.id.categoria);
                    tv.setTextColor(v.getResources().getColor(R.color.colorWhite));
                }
                else {
                    holder.lineSelected = false;
                    v.setBackgroundColor(v.getResources().getColor(R.color.quaseBranco));
                    TextView tv = v.findViewById(R.id.categoria);
                    tv.setTextColor(v.getResources().getColor(R.color.black));
                }
                return false;
            }
        });
    }




//    public static class CategoriaHolder {
//        boolean lineSelected = false;
//        //boolean checkboxSelected;
//        ConstraintLayout linha;
//        Categoria categoria;
//        TextView name;
//        LinearLayout check;
//        //CheckBox checkBox;
//    }
}



//        Categoria categoria = items.get(position);
//        if (categoria != null) {
//            TextView categoriaNameLabel = (TextView) v.findViewById(R.id.title);
//            if (categoriaNameLabel != null) {
//                categoriaNameLabel.setText(categoria.getTitle());
//            }
//        }

//        holder.button = (ImageView) v.findViewById(R.id.button);
//        holder.button.setTag(holder.categoria);
// holder.linha.setTag(holder.categoria);
//        holder.lineSelected= false;
//        holder.linha.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                holder.lineSelected = true;
//                v.setBackgroundColor(v.getResources().getColor(R.color.colorLightGrey));
//                return false;
//            }
//        });
// setupItem(holder);
//    private void setupItem(CategoriaHolder holder) {
//        holder.name.setText(holder.categoria.getTitle());
//    }

