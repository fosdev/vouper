package br.com.httpfluidobjects.vouper.Types;

import android.content.Context;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Outros.JSONComunication;

public class PerfilUsuario {

    private String nome;
    private String cpf;
    private String email;
    private String senha;
    private boolean permitirLocalizacao;
    private Bitmap imagem;
    private String token;
    private int id;

    public PerfilUsuario(String nome, String email, String cpf, String senha) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.senha = senha;
        token = null;
        imagem=null;
        id = 0;
    }
    public PerfilUsuario(){}

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public String getSenha() {
        return senha;
    }

    public boolean isPermitirLocalizacao() {
        return permitirLocalizacao;
    }

    public Bitmap getImagem() {
        return imagem;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setPermitirLocalizacao(boolean permitirLocalizacao) {
        this.permitirLocalizacao = permitirLocalizacao;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setImagem(Bitmap imagem) {
        this.imagem = imagem;
    }

    public void setId (int id){this.id = id;}

    public String getToken() {
        return token;
    }

    public int getId() {
        return id;
    }


    public void saveUser(Context context){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_NAME", nome).apply();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_PASS", senha).apply();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_EMAIL", email).apply();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_CPF", cpf).apply();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_TOKEN", token).apply();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("USER_ID", id).apply();
        if(imagem!= null){saveImage(context,imagem);}
    }

    public void saveImage(Context context, Bitmap image){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("USER_FOTO", imageEncoded).apply();
    }

    public static PerfilUsuario getSavedPerfil(Context context){
        PerfilUsuario savedUser= new PerfilUsuario(
                PreferenceManager.getDefaultSharedPreferences(context).getString("USER_NAME",""),
                PreferenceManager.getDefaultSharedPreferences(context).getString("USER_EMAIL",""),
                PreferenceManager.getDefaultSharedPreferences(context).getString("USER_CPF",""),
                PreferenceManager.getDefaultSharedPreferences(context).getString("USER_PASS","")
        );
        savedUser.setToken(PreferenceManager.getDefaultSharedPreferences(context).getString("USER_TOKEN",""));
        savedUser.setId(PreferenceManager.getDefaultSharedPreferences(context).getInt("USER_ID",0));
        return savedUser;
    }

    public boolean criarPerfilServer(){
        try {
            String s = toJSON().toString();
            String response = JSONComunication.postAdmin("http://vouper.staging.fluidobjects.com/entity/user?_format=json",s);
            if(!response.equals("-1")){
                //Salva o id em shared preferences
                JSONObject jsonObject = new JSONObject(response);
                JSONArray ja = jsonObject.getJSONArray("uid");
                this.id = Integer.parseInt(ja.getJSONObject(0).getString("value"));
                JSONArray ja2 = jsonObject.getJSONArray("uuid");
                this.token = ja2.getJSONObject(0).getString("value");
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void getUserInfo(Context context, String id, String userName, String pass){
        try {
            String response = JSONComunication.get("http://vouper.staging.fluidobjects.com/user/" + id + "?_format=hal_json");
            if(!response.equals("-1")){
                JSONObject jsonObject = new JSONObject(response);

                JSONArray ja = jsonObject.getJSONArray("mail");
                String email = ja.getJSONObject(0).getString("value");
                JSONArray ja2 = jsonObject.getJSONArray("field_cpf");
                String cpf = ja2.getJSONObject(0).getString("value");
                JSONArray ja3 = jsonObject.getJSONArray("uuid");
                String token = ja3.getJSONObject(0).getString("value");
                PerfilUsuario perfilUsuario = new PerfilUsuario(userName,email,cpf,pass);
                perfilUsuario.setId(Integer.parseInt(id));
                perfilUsuario.setToken(token);
                perfilUsuario.saveUser(context);
                //PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("USER_ID", ja2.getJSONObject(0).getInt("value")).apply();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject toJSON() throws JSONException {
        JSONObject jobj = new JSONObject();

        JSONObject href = new JSONObject();
        href.put("href", "http://vouper.staging.fluidobjects.com/rest/type/user/user");
        JSONObject type = new JSONObject();
        type.put("type",href);
        JSONObject links = new JSONObject();
        links.put("_links",type);

        JSONObject name = new JSONObject();
        name.put("value", nome);
        JSONObject cpf = new JSONObject();
        cpf.put("value",this.cpf);
        JSONObject mail = new JSONObject();
        mail.put("value", email);
        JSONObject roles = new JSONObject();
        roles.put("target_id", "authenticated");
        JSONObject senha = new JSONObject();
        senha.put("value", this.senha);
        JSONObject status = new JSONObject();
        status.put("value", 1);

        jobj.put("_links",type);
        jobj.put("name",name);
        jobj.put("mail",mail);
        jobj.put("pass",senha);
        jobj.put("roles",roles);
        jobj.put("field_cpf",cpf);
        jobj.put("status",status);
        return jobj;
    }
}
