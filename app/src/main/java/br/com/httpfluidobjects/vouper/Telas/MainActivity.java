package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import br.com.httpfluidobjects.vouper.Outros.MyDBhelper;
import br.com.httpfluidobjects.vouper.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MyDBhelper.criaBancos(this);
    }


    public void permitirEAnunciar(View v){
        Intent intent = new Intent(MainActivity.this, PermitirEAnunciarActivity.class);
        startActivity(intent);
    }

    public void anunciosPermitidos(View v){
        Intent intent = new Intent(MainActivity.this, CaixaDeEntradaActivity.class);
        startActivity(intent);
    }
}
