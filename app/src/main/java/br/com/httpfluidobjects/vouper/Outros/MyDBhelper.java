package br.com.httpfluidobjects.vouper.Outros;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import br.com.httpfluidobjects.vouper.Types.Categoria;

/**
 * Created by hermestessaro on 15/01/2018.
 */

public class MyDBhelper extends SQLiteOpenHelper implements BaseColumns {

    public static final String DB_NAME = "glasart.db";
    public static final String TABLE_NAME = "equipments";
    private static MyDBhelper sInstance;

    public MyDBhelper(Context context){
        super(context, DB_NAME, null, 1);
    }

    public static void criaBancos(Context context){
        SQLiteDatabase db = MyDBhelper.getsInstance(context).getWritableDatabase();
        db.execSQL("CREATE TABLE IF NOT EXISTS categorias(" +
                "id INTEGER UNIQUE PRIMARY KEY," +
                "title TEXT);");

        db.execSQL("CREATE TABLE IF NOT EXISTS anuncios(" +
                "id INTEGER PRIMARY KEY," +
                "level INTEGER," +
                "hysteresis INTEGER," +
                "logInterval INTEGER," +

                "onTimeLvl1 INTEGER," +
                "offTimeLvl1 INTEGER," +
                "temperatureLvl1 INTEGER," +

                "onTimeLvl2 INTEGER," +
                "offTimeLvl2 INTEGER," +
                "temperatureLvl2 INTEGER," +

                "onTimeLvl3 INTEGER," +
                "offTimeLvl3 INTEGER," +
                "temperatureLvl3 INTEGER);"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS agendamento(" +
                "id INTEGER PRIMARY KEY," +
                "onTime1 TEXT," +
                "offTime1 TEXT," +
                "setPoint1 INTEGER," +
                "days1 TEXT," +
                "enabled1 BOOLEAN," +

                "onTime2 TEXT," +
                "offTime2 TEXT," +
                "setPoint2 INTEGER," +
                "days2 TEXT," +
                "enabled2 BOOLEAN," +

                "onTime3 TEXT," +
                "offTime3 TEXT," +
                "setPoint3 INTEGER," +
                "days3 TEXT," +
                "enabled3 BOOLEAN);"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS logs(" +
                "id INTEGER," +
                "time INTEGER ," +
                "programedTemp TEXT," +
                "currentTemp TEXT," +
                "heaterOn BOOLEAN," +
                "PRIMARY KEY(time, id));"
        );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public static synchronized MyDBhelper getsInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new MyDBhelper(context.getApplicationContext());
        }
        return sInstance;
    }


    public static int InsertCategoria(Context context, Categoria categoria){
        SQLiteDatabase db = MyDBhelper.getsInstance(context).getWritableDatabase();
        final String TABLE_NAME = "categorias";
        ContentValues catValues = new ContentValues();

        catValues.put("id", categoria.getId());
        catValues.put("title", categoria.getTitle());
        int result  = (int)db.insert(TABLE_NAME, null, catValues);
        db.close();
        return result;
    }


//    public static Equipamento returnsEquip(SQLiteDatabase db, int id){
//        String query = "SELECT * FROM equipments WHERE id="+id+";";
//        Cursor cursor      = db.rawQuery(query, null);
//        if(cursor.moveToFirst()){
//        int id_db = cursor.getInt(cursor.getColumnIndex("id"));
//        String name = cursor.getString(cursor.getColumnIndex("name"));
//        String ip = cursor.getString(cursor.getColumnIndex("ip"));
//        String string = cursor.getString(cursor.getColumnIndex("serial"));
//        String size = cursor.getString(cursor.getColumnIndex("size"));
//        int c_temp = cursor.getInt(cursor.getColumnIndex("current_temperature"));
//        int p_temp = cursor.getInt(cursor.getColumnIndex("programmed_temperature"));
//        boolean assoc = cursor.getInt(cursor.getColumnIndex("associated")) > 0;
//        boolean isOn = cursor.getInt(cursor.getColumnIndex("isOn")) > 0;
//        boolean conn = cursor.getInt(cursor.getColumnIndex("connected")) > 0;
//        boolean operating = cursor.getInt(cursor.getColumnIndex("operating")) > 0;
//        String device_id = cursor.getString(cursor.getColumnIndex("device_id"));
//        Equipamento aux = new Equipamento(id_db,ip, name, string, size, c_temp, p_temp, assoc, isOn, conn, operating, device_id);
//        cursor.close();
//        return aux;
//        }return null;
//    }

//    public static void updateEquip(Context context,Equipamento equip){
//        SQLiteDatabase db;
//        db = MyDBhelper.getsInstance(context).getWritableDatabase();
//
//        ContentValues value = new ContentValues();
//
//        value.put("name", equip.getName());
//        value.put("ip", equip.getIp());
//        value.put("serial", equip.getSerial_number());
//        value.put("size", equip.getSize());
//        value.put("current_temperature", equip.getCurrent_temp());
//        value.put("programmed_temperature", equip.getProgrammed_temp());
//        value.put("associated", equip.isAssociated());
//        value.put("isOn", equip.isOn());
//        value.put("connected", equip.isConnected());
//        value.put("operating", equip.isOperating());
//        value.put("device_id", equip.getDeviceId());
//        String where = "id = "+ equip.getId() + ";";
//        db.update(TABLE_NAME,value,where,null);
//        //db.close();  essa linha tava bugando o polling
//    }

//    public static void updateEquipIp(Context context,Equipamento equip){
//        SQLiteDatabase db;
//        db = MyDBhelper.getsInstance(context).getWritableDatabase();
//        String query = "UPDATE equipments SET ip = " + equip.getIp() + " WHERE id = " + equip.getId() + ";";
//        db.execSQL(query);
//    }

    public static ArrayList<Categoria> getCategorias(Context context){
        ArrayList<Categoria> response = new ArrayList<Categoria>();
        SQLiteDatabase db;
        db = MyDBhelper.getsInstance(context).getWritableDatabase();
        String query = "SELECT * FROM categorias;";
        Cursor cursor      = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                int id_db = cursor.getInt(cursor.getColumnIndex("id"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                response.add(new Categoria(id_db,title));
            }while(!cursor.isLast() && cursor.moveToNext());
        }
        else return new ArrayList<Categoria>();
        return response;
    }

    public static ArrayList<String> getTitleCategorias(Context context){
        ArrayList<String> response = new ArrayList<String>();
//        response.add("cccccc");
        SQLiteDatabase db;
        db = MyDBhelper.getsInstance(context).getWritableDatabase();
        String query = "SELECT * FROM categorias;";
        Cursor cursor      = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                String s = cursor.getString(cursor.getColumnIndex("title"));
                response.add(s);
            }while(!cursor.isLast() && cursor.moveToNext());
        }
        else return response;
        return response;
    }
}
