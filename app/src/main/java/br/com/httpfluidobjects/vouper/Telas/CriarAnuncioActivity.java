package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.httpfluidobjects.vouper.Types.Anuncio;
import br.com.httpfluidobjects.vouper.R;

public class CriarAnuncioActivity extends AppCompatActivity {

    ArrayList <Integer> idCategoria = new ArrayList<>();
    ArrayList <String> titleCategoria = new ArrayList<>();
    int i ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_anuncio);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        idCategoria = getIntent().getIntegerArrayListExtra("idCategoria");
        titleCategoria = getIntent().getStringArrayListExtra("titleCategoria");
        i=0;

        TextView categoria = findViewById(R.id.categoria);
        categoria.setText(titleCategoria.get(i));
        Button btn = findViewById(R.id.botaoAnunciar);
        if (i >= titleCategoria.size()-1) btn.setText("Proximo");
        else btn.setText("Terminar");
    }

    public void submit(View v){

        EditText preco = findViewById(R.id.precoInput);
        EditText alcance = findViewById(R.id.alcanceInput);
        EditText cidade = findViewById(R.id.cidadeInput);
        EditText duracao = findViewById(R.id.duracaoInput);
        EditText descricao = findViewById(R.id.descricaoInput);

       // PerfilUsuario user = PerfilUsuario.getUserInfo(this,40);
        //user.setId(39);

        int id = PreferenceManager.getDefaultSharedPreferences(this).getInt("USER_ID",0);
        Anuncio anuncio = new Anuncio(
                id,
                idCategoria.get(i),
                Integer.parseInt(duracao.getText().toString()),
                0,
                0,
                Float.parseFloat(preco.getText().toString()),
                Float.parseFloat(alcance.getText().toString()),
                cidade.getText().toString(),
                "localizacao (BETA)",
                descricao.getText().toString());

        anuncio.postToServer(this);
        if (i >= titleCategoria.size()-1) {
            Intent intent = new Intent( CriarAnuncioActivity.this, MainActivity.class);
            startActivity(intent);
        }else recarregaPagina();
    }

    private void recarregaPagina(){
        i++;
        TextView categoria = findViewById(R.id.categoria);
        categoria.setText(titleCategoria.get(i));

        EditText preco = findViewById(R.id.precoInput);
        EditText alcance = findViewById(R.id.alcanceInput);
        EditText cidade = findViewById(R.id.cidadeInput);
        EditText duracao = findViewById(R.id.duracaoInput);
        EditText descricao = findViewById(R.id.descricaoInput);
        Button btn = findViewById(R.id.botaoAnunciar);

        if (i >= titleCategoria.size()-1) btn.setText("Proximo");
        else btn.setText("Terminar");

        preco.setText("");
        alcance.setText("");
        cidade.setText("");
        duracao.setText("");
        descricao.setText("");
    }
}
