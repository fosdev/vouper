package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Outros.AdapterPermicoes;
import br.com.httpfluidobjects.vouper.Outros.CategoriaHolder;
import br.com.httpfluidobjects.vouper.Outros.MyDBhelper;
import br.com.httpfluidobjects.vouper.Types.Categoria;
import br.com.httpfluidobjects.vouper.Outros.JSONComunication;
import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;
import br.com.httpfluidobjects.vouper.R;

public class PermitirEAnunciarActivity extends AppCompatActivity {

    private ArrayList<Integer> indicesCategorias = new ArrayList<Integer>();
    private ArrayList<Integer> indicesChecked = new ArrayList<Integer>();
    private ArrayList<Integer> indicesSelecionados = new ArrayList<Integer>();
    private ArrayList<String> categoriasFiltradasTitle = new ArrayList<String>();
    //ArrayAdapter<String> adapter;
    AdapterPermicoes adapter;
    Thread t;
    String lastText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permitir_eanunciar);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        t=new Thread();
        lastText = "";
        configureSearchField();
    }

    public void irMenuInicial(View view){
        Intent intent = new Intent(PermitirEAnunciarActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void selectedBox(View v) {
        Categoria cat = (Categoria) v.getTag();
        int id = cat.getId();
        if (!indicesChecked.contains(id)){
            v.setBackground(v.getResources().getDrawable(R.drawable.checked_icon));
            indicesChecked.add(id);
        }
        else {
            v.setBackground(v.getResources().getDrawable(R.drawable.borda_verde));
            indicesChecked.remove(indicesChecked.indexOf(id));
        }
    }


    private void configureSearchField() {
        adapter = new AdapterPermicoes(this, new ArrayList<Categoria>());
        //adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line);
        EditText txt = findViewById(R.id.pesquisa);
        final ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setLongClickable(true);
        listView.clearFocus();

        txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                if (!s.toString().trim().equals("")) {
                   Log.d("Tag","after"+s.toString());
                   getCategorias(s.toString(), 10, 0);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {}
        });

//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                if (!indicesSelecionados.contains(id)){
//                    view.setBackgroundColor(getResources().getColor(R.color.vouperFundo));
//                    TextView tv = view.findViewById(R.id.categoria);
//                    tv.setTextColor(getResources().getColor(R.color.colorWhite));
//                    indicesSelecionados.add((int)id);
//                }
//                else {
//                    view.setBackgroundColor(getResources().getColor(R.color.quaseBranco));
//                    TextView tv = view.findViewById(R.id.categoria);
//                    tv.setTextColor(getResources().getColor(R.color.black));
//                    indicesSelecionados.remove(indicesSelecionados.indexOf(id));
//                }
//
//
//                Intent intent = new Intent(PermitirEAnunciarActivity.this, CriarAnuncioActivity.class);
//                intent.putExtra("idCategoria",indicesCategorias.get((int) id));
//                intent.putExtra("titleCategoria",categoriasFiltradasTitle.get((int)id));
//                //startActivity(intent);
//                return false;
//            }
//        });
    }

    public void anunciar(View v){
        ArrayList<Integer> idsSelected = new ArrayList<>();
        ArrayList<String> titlesSelected = new ArrayList<>();
        ListView listView = findViewById(R.id.list);
        for(int i=0; i < listView.getAdapter().getCount();i++){
            View view = listView.getChildAt(i);
            if(view!=null) {
                Object o = view.getTag();
                CategoriaHolder holder = (CategoriaHolder) o;
                if (holder.lineSelected){
                    titlesSelected.add(categoriasFiltradasTitle.get(i));
                    idsSelected.add(indicesCategorias.get(i));
                }
                // view.setBackground(getResources().getDrawable(R.drawable.small_chat_icon));
            }
        }
        if(!idsSelected.isEmpty()) {
            Intent intent = new Intent(PermitirEAnunciarActivity.this, CriarAnuncioActivity.class);
            intent.putExtra("idCategoria",idsSelected);
            intent.putExtra("titleCategoria",titlesSelected);
            startActivity(intent);
        }
    }


    //pega as categorias e salva no banco
    private void getCategorias(final String txt, final int itensPorPag, final int pagina) {
        final Handler mHandler = new Handler();
        t.interrupt();
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                final ArrayList<Categoria> categorias = new ArrayList<Categoria>();
                try {
                    lastText = txt;
                    Thread.sleep(400);
                    String offset = String.valueOf(itensPorPag * pagina);
                    String n = String.valueOf(itensPorPag);

                    PerfilUsuario user = PerfilUsuario.getSavedPerfil(getApplicationContext());
                    if(txt.equals(lastText)) {
                        String response = JSONComunication.getAuthenticated("http://vouper.staging.fluidobjects.com/api/get_categorias?_format=hal_json&items_per_page=" + n + "&offset=" + offset + "&title=" + txt, user.getNome(), user.getSenha());
                        Log.d("Chegou Aqui2!", txt);
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int id = jsonArray.getJSONObject(i).getInt("nid");
                            Categoria categoria = new Categoria(id, (String) jsonArray.getJSONObject(i).get("title"));
                            categorias.add(categoria);
                            //MyDBhelper.InsertCategoria(getApplicationContext(), categoria);
                        }

                        //Log.d("Chegou Aqui3!", txt);
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                updateAdapter(categorias);
                            }
                        });
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
        t.start();
    }

    private void updateAdapter(ArrayList<Categoria> categorias) {
        adapter.clear();
        indicesCategorias.clear();
        categoriasFiltradasTitle.clear();
        indicesChecked.clear();
        indicesSelecionados.clear();

        for (Categoria categoria : categorias) {
            categoriasFiltradasTitle.add(categoria.getTitle());
            indicesCategorias.add(categoria.getId());
        }
        adapter.addAll(categorias);
    }

}