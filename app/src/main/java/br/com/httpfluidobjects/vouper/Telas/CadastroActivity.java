package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import br.com.httpfluidobjects.vouper.R;
import br.com.httpfluidobjects.vouper.Outros.Validations;

public class CadastroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void continuar(View v){
        EditText email = findViewById(R.id.userInput);
        EditText senha1 = findViewById(R.id.senhaInput);
        EditText senha2 = findViewById(R.id.senhaConfirmInput);


        if(Validations.validateEmail(email.getText().toString())){
            if(Validations.validateSenha(senha1.getText().toString())){
                if(Validations.validateSenhaConfirma(senha1.getText().toString(),senha2.getText().toString())){
                    Intent intent = new Intent(CadastroActivity.this, Cadastro2Activity.class);
                    intent.putExtra("email", email.getText().toString());
                    intent.putExtra("senha1", senha1.getText().toString());
                    intent.putExtra("senha2", senha2.getText().toString());
                    startActivity(intent);
                }
            }
        }
    }
}
