package br.com.httpfluidobjects.vouper.Telas;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

import br.com.httpfluidobjects.vouper.Outros.JSONComunication;
import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;
import br.com.httpfluidobjects.vouper.R;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


    }

    public void confirmar(View v){
        //String response = JSONComunication.login();
        EditText fieldUser = findViewById(R.id.userInput);
        EditText fieldSenha = findViewById(R.id.senhaInput);
        String response = JSONComunication.login(fieldUser.getText().toString(), fieldSenha.getText().toString());
        if(!response.equals("-1")){
            PerfilUsuario.getUserInfo(this,response,fieldUser.getText().toString(), fieldSenha.getText().toString());
            PreferenceManager.getDefaultSharedPreferences(this).edit().putInt("USER_ID",Integer.parseInt(response)).apply();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }else{

        }
    }
}
