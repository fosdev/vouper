package br.com.httpfluidobjects.vouper.Outros;

import android.support.constraint.ConstraintLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.httpfluidobjects.vouper.Types.Categoria;

public class CategoriaHolder {
    public boolean lineSelected = false;
    //boolean checkboxSelected;
    public ConstraintLayout linha;
    public Categoria categoria;
    public TextView name;
    public LinearLayout check;
    //CheckBox checkBox;
}
