package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;

import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;
import br.com.httpfluidobjects.vouper.R;
import br.com.httpfluidobjects.vouper.Outros.Validations;
import de.hdodenhof.circleimageview.CircleImageView;

public class Cadastro2Activity extends AppCompatActivity implements IPickResult {

    Bitmap foto;
    EditText nomeField;
    EditText cpfField;
    String email;
    String senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro2);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = getIntent();
        email = intent.getStringExtra("email");
        senha = intent.getStringExtra("senha1");


        nomeField = findViewById(R.id.nomeField);
        cpfField = findViewById(R.id.cpfField);
        foto = BitmapFactory.decodeResource(getResources(), R.drawable.user_default);
    }

    public void chooseImage(View view){
        PickImageDialog.build(new PickSetup().setButtonOrientation(LinearLayout.HORIZONTAL)).show(this);
    }


    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {

            foto = r.getBitmap();
            CircleImageView imagem = (CircleImageView) findViewById(R.id.profile_image);
            imagem.setImageBitmap(r.getBitmap());
        } else {
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void finalizarCadastro(View view) {
        String nome = nomeField.getText().toString();
        String cpf = cpfField.getText().toString();

         if(validarDados(nome, cpf)){
        //if(true){
             PerfilUsuario user = new PerfilUsuario(nome,email,cpf,senha);
             user.setImagem(foto);
             if(user.criarPerfilServer()) {
                 user.saveUser(this);
                 Intent intent = new Intent(Cadastro2Activity.this, MainActivity.class);
                 startActivity(intent);
             }
         }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private boolean validarDados(String nome, String cpf){
        boolean valido = true;

        if(!Validations.validateNome(nome)){
            //txtErroNome.setText("Nome inválido");
            valido = false;
        }//else txtErroNome.setText("");

        if(!Validations.validateEmail(email)){
           // txtErroEmail.setText("Email inválido");
            valido = false;
        }//else txtErroEmail.setText("");

//        if(!Validations.validateCpf(cpf)){
           // txtErroCpf.setText("CPF inválido");
//            valido = false;
//        }//else txtErroCpf.setText("");
        return valido;
    }

}
