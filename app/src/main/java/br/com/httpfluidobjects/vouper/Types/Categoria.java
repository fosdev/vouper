package br.com.httpfluidobjects.vouper.Types;

import java.text.Normalizer;

public class Categoria {

    private int id;
    private String title;

    public Categoria(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleSemAcento() {
        return  Normalizer.normalize(title , Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
