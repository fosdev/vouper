package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Outros.JSONComunication;
import br.com.httpfluidobjects.vouper.R;

public class CaixaDeEntradaActivity extends AppCompatActivity {

    ArrayList<String> titleAnuncios = new ArrayList<String>();
    ArrayList<String> idAnuncios = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caixa_de_entrada);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,titleAnuncios);
        ListView listView = findViewById(R.id.listaAnuncios);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CaixaDeEntradaActivity.this, VisualizarAnuncioActivity.class);
                intent.putExtra("idAnuncio", idAnuncios.get(position));
                startActivity(intent);
            }
        });
        configureListView();
    }

    private void configureListView(){
        try {
            String response = JSONComunication.get("http://vouper.staging.fluidobjects.com/get_anuncios");
            if(!response.equals("-1")) {
                JSONArray jsonArray = null;
                jsonArray = new JSONArray(response);
                titleAnuncios.clear();
                idAnuncios.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    String s = jsonArray.getJSONObject(i).getString("field_categoria_1");
                    String id = jsonArray.getJSONObject(i).getString("nid");
                    if(!s.equals(""))
                    titleAnuncios.add(s);
                    if(!id.equals(""))
                        idAnuncios.add(id);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}
