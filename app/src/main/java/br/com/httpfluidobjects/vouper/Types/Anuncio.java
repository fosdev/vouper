package br.com.httpfluidobjects.vouper.Types;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Outros.JSONComunication;

public class Anuncio {

    private int idAnunciante;
    private int categoria;
    private int duracao;
    private int rascunho;
    private int vendido;
    private float preco;
    private float alcance;
    private int dataPublicacao;
    private String descricao;
    private String cidade;
    private String localizacao;
    private Bitmap[] imagens;
    private String nomeCategoria;
    private String nomeAnunciante;



    public Anuncio(int idAnunciante, int categoria, int duracao, int rascunho, int vendido, float preco, float alcance, String cidade, String localizacao, String descricao) {
        this.idAnunciante = idAnunciante;
        this.categoria = categoria;
        this.duracao = duracao;
        this.rascunho = rascunho;
        this.vendido = vendido;
        this.preco = preco;
        this.alcance = alcance;
       // this.dataPublicacao = dataPublicacao;
        this.descricao = descricao;
        this.cidade = cidade;
        this.localizacao = localizacao;
    }
    public Anuncio(int idAnunciante, int categoria,int rascunho, int vendido, float preco) {
        this.idAnunciante = idAnunciante;
        this.categoria = categoria;
        this.rascunho = rascunho;
        this.vendido = vendido;
        this.preco = preco;
    }

    public Anuncio(String idAnuncio){
        try {
            String response = JSONComunication.get("http://vouper.staging.fluidobjects.com/get_anuncios?format=json&nid=" + idAnuncio);
            JSONObject jobj = new JSONArray(response).getJSONObject(0);

            preco = Float.parseFloat(jobj.getString("field_preco"));
            descricao = jobj.getString("field_descricao");
            duracao = jobj.getInt("field_duracao");
            nomeAnunciante = jobj.getString("field_anunciante");
            nomeCategoria = jobj.getString("field_categoria_2");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public String getNomeAnunciante() {
        return nomeAnunciante;
    }

    public int getIdAnunciante() {
        return idAnunciante;
    }

    public void setIdAnunciante(int idAnunciante) {
        this.idAnunciante = idAnunciante;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public float getRascunho() {
        return rascunho;
    }

    public void setRascunho(int rascunho) {
        this.rascunho = rascunho;
    }

    public float getVendido() {
        return vendido;
    }

    public void setVendido(int vendido) {
        this.vendido = vendido;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public float getAlcance() {
        return alcance;
    }

    public void setAlcance(float alcance) {
        this.alcance = alcance;
    }

    public int getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(int dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public Bitmap[] getImagens() {
        return imagens;
    }

    public void setImagens(Bitmap[] imagens) {
        this.imagens = imagens;
    }

    private JSONObject toJSON() throws JSONException {
        JSONObject jobj = new JSONObject();

        JSONObject href = new JSONObject();
        href.put("href", "http://vouper.staging.fluidobjects.com/rest/type/node/anuncio");
        JSONObject type = new JSONObject();
        type.put("type",href);
        JSONObject links = new JSONObject();
        links.put("_links",type);

        JSONObject title = new JSONObject();
        title.put("value", "Anuncio pelo App");

        JSONObject preco = new JSONObject();
        preco.put("value",this.preco);

        JSONObject alcance = new JSONObject();
        alcance.put("value",this.alcance);
        JSONObject cidade = new JSONObject();
        cidade.put("value",this.cidade);
        JSONObject descricao = new JSONObject();
        descricao.put("value",this.descricao);

        JSONObject duracao = new JSONObject();
        duracao.put("value",this.duracao);

        JSONObject date = new JSONObject();
        date.put("value", "2018-06-14T12:05:19+00:00");
        date.put("format", "Y-m-d\\\\TH:i:sP");
        JSONArray arrayDate = new JSONArray();
        arrayDate.put(date);

        JSONObject anunciante = new JSONObject();
        anunciante.put("target_id", idAnunciante);
        JSONArray arrayAnunciante = new JSONArray();
        arrayAnunciante.put(anunciante);

        JSONObject categ = new JSONObject();
        categ.put("target_id", categoria);
        JSONArray arrayCateg = new JSONArray();
        arrayCateg.put(categ);

        JSONObject type2 = new JSONObject();
        type2.put("target_id", "anuncio");

        jobj.put("_links",type);
        jobj.put("title",title);
        jobj.put("field_preco",preco);
        jobj.put("type",type2);
        jobj.put("field_alcance_do_anuncio",alcance);
        jobj.put("field_cidade",cidade);
        jobj.put("field_descricao",descricao);
        jobj.put("field_duracao",duracao);
        jobj.put("field_data_de_publicacao",arrayDate);
        jobj.put("field_anunciante",arrayAnunciante);
        jobj.put("field_categoria",arrayCateg);
        return jobj;
    }

    public boolean postToServer(Context context){
        try {
            String s = toJSON().toString();
            PerfilUsuario user = PerfilUsuario.getSavedPerfil(context);
            String response = JSONComunication.postAuthenticated("http://vouper.staging.fluidobjects.com/entity/node?_format=hal_json",s,user);
            if(!response.equals("-1")){
                return true;
            }else{
                //TODO:Salva offline como rascunho
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }
}
