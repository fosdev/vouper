package br.com.httpfluidobjects.vouper.Telas;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Types.Anuncio;
import br.com.httpfluidobjects.vouper.Outros.JSONComunication;
import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;
import br.com.httpfluidobjects.vouper.R;

public class VisualizarAnuncioActivity extends Activity {

    String idAnuncio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_anuncio);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        idAnuncio = getIntent().getStringExtra("idAnuncio");
        carregarDados();
    }

    private void carregarDados(){
        final Handler mHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Anuncio anuncio = new Anuncio(idAnuncio);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView desc = findViewById(R.id.descAnuncio);
                        TextView cat = findViewById(R.id.nomeAnuncio);
                        TextView preco = findViewById(R.id.precoAnuncio);
                        TextView anunciante = findViewById(R.id.anunciante);
                        TextView duracao = findViewById(R.id.dataExpira);
                        desc.setText(desc.getText() + ":  " + anuncio.getDescricao());
                        cat.setText(cat.getText() + ":  " + anuncio.getNomeCategoria());
                        preco.setText(preco.getText() + ":  " + anuncio.getPreco());
                        anunciante.setText(anunciante.getText() + ":  " + anuncio.getNomeAnunciante());
                        duracao.setText(duracao.getText() + ":  " + anuncio.getDuracao());
                    }
                });
            }
        }).start();
    }

    public void comprar(View view){
        PerfilUsuario user = PerfilUsuario.getSavedPerfil(this);
        String data;
        data = "{\"_links\":{\"type\": {\"href\": \"http://vouper.staging.fluidobjects.com/rest/type/node/venda\"}},\n" +
                "\"title\": [{\"value\": \"Venda App\"}],\n" +
                "\"type\": [{\"target_id\": \"venda\"}],\n" +
                "\"field_anuncio\":[{\"target_id\": " + idAnuncio + " }],\n" +
                "\"field_comprador\":[{\"target_id\":" + user.getId() + " }]\n" +
                "}\n";
        try {
            String response = JSONComunication.postAuthenticated("http://vouper.staging.fluidobjects.com/entity/node?_format=hal_json",data,user);
            if(!response.equals("-1")){
                Intent intent = new Intent(VisualizarAnuncioActivity.this, MainActivity.class);
                startActivity(intent);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
