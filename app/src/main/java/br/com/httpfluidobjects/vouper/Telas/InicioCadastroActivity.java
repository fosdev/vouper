package br.com.httpfluidobjects.vouper.Telas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;
import br.com.httpfluidobjects.vouper.R;

public class InicioCadastroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_cadastro);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if(PerfilUsuario.getSavedPerfil(this).getId() != 0){
            Intent intent = new Intent(InicioCadastroActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void cadastrar(View view){
        Intent intent = new Intent(InicioCadastroActivity.this, CadastroActivity.class);
        startActivity(intent);
    }

    public void apenasNavegar(View view){
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt("USER_ID", 0).apply();
        Intent intent = new Intent(InicioCadastroActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void fazerLogin(View view){
        Intent intent = new Intent(InicioCadastroActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}