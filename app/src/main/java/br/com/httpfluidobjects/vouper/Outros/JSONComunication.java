package br.com.httpfluidobjects.vouper.Outros;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;

/**
 * Created by hermestessaro on 20/12/2017.
 */

public class JSONComunication {


    public static String postAuthenticated(final String url, final String data,final PerfilUsuario user) throws ExecutionException, InterruptedException {

        class SendJSON extends AsyncTask<String, Void, String> {
            String response = "-1";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {


                URL url1;
                HttpURLConnection conn = null;

                try {
                    url1 = new URL(url);
                    conn = (HttpURLConnection) url1.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/hal+json");
                    //conn.setRequestProperty("X-CSRF-Token", "zXoppSNqM21RbGVHjv8YgKKIcEvhdc_v0XqGx_i-dkY");
                    String auth =new String(user.getNome() + ":" + user.getSenha());
                    byte[] data1 = auth.getBytes();
                    String base64 = Base64.encodeToString(data1, Base64.NO_WRAP);
                    conn.setRequestProperty("Authorization","Basic "+base64);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(data);
                    Log.d("TAG","Vai escrever");
                    writer.flush();
                    writer.close();
                    os.close();
                    Log.d("TAG","Escreveu");
                    int responseCode=conn.getResponseCode();
                    String line;
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    sb.append("");
                    response = sb.toString().trim();
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    response = "-1";
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
                return response;
            }

            public String getResponse(){
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }
        SendJSON sj = new SendJSON();
        sj.execute(url).get();
        return sj.getResponse();
    }


    //Usada para criar um novo usuario
    //Retorna o X-CSRF-Token do novo usuario criado
    //Em caso de erro, retorna -1
    public static String postAdmin(final String url, final String data) throws ExecutionException, InterruptedException {

        class SendJSON extends AsyncTask<String, Void, String> {
            String response = "-1";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                URL url1;
                HttpURLConnection conn = null;
                try {
                    url1 = new URL(url);
                    conn = (HttpURLConnection) url1.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/hal+json");
                    //conn.setRequestProperty("X-CSRF-Token", "XIvtzqAmJdjzrYa0CLTCaOxI6wq_zjKKxkOYRCgXLrc");
                    //conn.setRequestProperty("X-CSRF-Token", "PMAxl2Ciie6WAVk-kiJPkD9vBKsnoy0NoSpdTDa0REk");
                    String auth =new String("admin" + ":" + "Flu@2Obj!");
                    byte[] data1 = auth.getBytes();
                    String base64 = Base64.encodeToString(data1, Base64.NO_WRAP);
                    conn.setRequestProperty("Authorization","Basic "+base64);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(data);
                    Log.d("TAG","Vai escrever");
                    writer.flush();
                    writer.close();
                    os.close();
                    Log.d("TAG","Escreveu");
                    int responseCode=conn.getResponseCode();
                    String line;
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    sb.append("");
                    response = sb.toString().trim();
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    response = "-1";
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
                return response;
            }

            public String getResponse(){
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }
        SendJSON sj = new SendJSON();
        sj.execute(url).get();
        return sj.getResponse();
    }


    public static String get(final String url) throws ExecutionException, InterruptedException {

        class SendJSON extends AsyncTask<String, Void, String> {
            String response = "-1";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                URL url1;
                HttpURLConnection conn = null;
                try {
                    url1 = new URL(url);
                    conn = (HttpURLConnection) url1.openConnection();
                    conn.setConnectTimeout(1500);
                    conn.setRequestMethod("GET");
                    switch (conn.getResponseCode()) {
                        case HttpURLConnection.HTTP_OK:
                            Log.d("TAG"," **OK**");
                            break; // fine, go on
                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            Log.d("TAG"," ****gateway timeout****");
                            break;// retry
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.d("TAG"," **unavailable***");
                            break;// retry, server is unstable
                        default:
                            Log.d("TAG"," **unknown response code**.");
                            break; // abort
                    }
                    int responseCode=conn.getResponseCode();
                    Log.d("TAG","Leu" + String.valueOf(responseCode));
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        StringBuilder sb = new StringBuilder();
                        BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        while ((line=br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        sb.append("");
                        response = sb.toString().trim();
                        br.close();
                        Log.d("TAG","Resposta" + response);
                    }
                    else {
                        response = "-1";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    response = "-1";
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
                return response;
            }

            public String getResponse(){
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }
        SendJSON sj = new SendJSON();
        sj.execute(url).get();
        return sj.getResponse();
    }

    public static String getAuthenticated(final String url, final String userName, final String senha) throws ExecutionException, InterruptedException {

        class SendJSON extends AsyncTask<String, Void, String> {
            String response = "-1";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                URL url1;
                HttpURLConnection conn = null;

                try {
                    url1 = new URL(url);
                    conn = (HttpURLConnection) url1.openConnection();
                    conn.setConnectTimeout(1500);
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-Type", "application/hal+json");

                    String auth =new String(userName + ":" + senha);
                    byte[] data1 = auth.getBytes();
                    String base64 = Base64.encodeToString(data1, Base64.NO_WRAP);
                    conn.setRequestProperty("Authorization","Basic " + base64);

                    switch (conn.getResponseCode()) {
                        case HttpURLConnection.HTTP_OK:
                            Log.d("TAG"," **OK**");
                            break; // fine, go on
                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            Log.d("TAG"," ****gateway timeout****");
                            break;// retry
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.d("TAG"," **unavailable***");
                            break;// retry, server is unstable
                        default:
                            Log.d("TAG"," **unknown response code**.");
                            break; // abort
                    }
                    int responseCode=conn.getResponseCode();
                    Log.d("TAG","Leu" + String.valueOf(responseCode));
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        StringBuilder sb = new StringBuilder();
                        BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        while ((line=br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        sb.append("");
                        response = sb.toString().trim();
                        br.close();
                        Log.d("TAG","Resposta" + response);
                    }
                    else {
                        response = "-1";
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    response = "-1";
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
                return response;
            }

            public String getResponse(){
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }
        SendJSON sj = new SendJSON();
        sj.execute(url).get();
        return sj.getResponse();
    }

    public static String login(final String userName, final String pass) {

        class SendJSON extends AsyncTask<Void, Void, String> {
            String response = "-1";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                URL url1;
                HttpURLConnection conn = null;
                try {
                    url1 = new URL("http://vouper.staging.fluidobjects.com/user/login?_format=json");
                    conn = (HttpURLConnection) url1.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write("{\"name\": \""+ userName +"\",\"pass\": \"" + pass + "\"}");
                    Log.d("TAG","Vai escrever");
                    writer.flush();
                    writer.close();
                    os.close();
                    Log.d("TAG","Escreveu");
                    //int code = conn.getResponseCode();

                    String line;
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    sb.append("");
                    response = sb.toString().trim();
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    response = "-1";
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
                return response;
            }

            public String getResponse(){
                try {
                    JSONObject jobj = new JSONObject(response);
                    return jobj.getJSONObject("current_user").getString("uid");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "-1";
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }
        SendJSON sj = new SendJSON();
        try {
            sj.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return sj.getResponse();
    }
}
