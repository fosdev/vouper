package br.com.httpfluidobjects.vouper;

import org.junit.Test;

import br.com.httpfluidobjects.vouper.Outros.Validations;

import static org.junit.Assert.*;

public class ValidationsTest {

    @Test
    public void validateCpf() {
        assertFalse(Validations.validateCpf("23456789"));
        assertTrue(Validations.validateCpf("02013442033"));
        assertFalse(Validations.validateCpf("020134420333456"));
    }

    @Test
    public void validateEmail() {
        assertFalse(Validations.validateEmail("23456789"));
        assertFalse(Validations.validateEmail("rrrtttt"));
        assertFalse(Validations.validateEmail("rrrtttt@www"));
        assertTrue(Validations.validateEmail("ggg@ggg.huu"));
        assertTrue(Validations.validateEmail("rrrtttt@www.rrrr"));
    }

    @Test
    public void validateNome() {
        assertFalse(Validations.validateNome("01"));
        assertFalse(Validations.validateNome(""));
        assertTrue(Validations.validateNome("Super Tester"));
    }

    @Test
    public void validateSenha() {
        assertFalse(Validations.validateSenha(""));
        assertFalse(Validations.validateSenha("12345"));
        assertTrue(Validations.validateSenha("123456"));
    }

    @Test
    public void validateSenhaConfirma() {
        assertFalse(Validations.validateSenhaConfirma("12345678","1234"));
        assertTrue(Validations.validateSenhaConfirma("12345678","12345678"));
    }
}