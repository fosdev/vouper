package br.com.httpfluidobjects.vouper;

import android.content.Intent;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import br.com.httpfluidobjects.vouper.Outros.JSONComunication;
import br.com.httpfluidobjects.vouper.Telas.MainActivity;
import br.com.httpfluidobjects.vouper.Telas.VisualizarAnuncioActivity;
import br.com.httpfluidobjects.vouper.Types.PerfilUsuario;

import static org.junit.Assert.*;

public class JSONComunicationTest {

    @Test
    public void postAdmin() {
        String data;
        data = "{\"_links\":{\"type\": {\"href\": \"http://vouper.staging.fluidobjects.com/rest/type/node/venda\"}},\n" +
                "\"title\": [{\"value\": \"Venda App\"}],\n" +
                "\"type\": [{\"target_id\": \"venda\"}],\n" +
                "\"field_anuncio\":[{\"target_id\":  2977 }],\n" +
                "\"field_comprador\":[{\"target_id\":39}]\n" +
                "}\n";
        try {
            String response = JSONComunication.postAdmin("http://vouper.staging.fluidobjects.com/entity/node?_format=hal_json",data);
            assertNotEquals("-1", response);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void get() {
        try {
            String response = JSONComunication.get("http://www.google.com.br");
            assertNotEquals("-1", response);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            String response = JSONComunication.get("http://vouper.staging.fluidobjects.com");
            assertNotEquals("-1", response);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAutenticated() {
        try {
            String response = JSONComunication.getAuthenticated("http://vouper.staging.fluidobjects.com/api/get-users?_format=json","tester","123456");
            assertNotEquals("-1",response);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void postAutenticated() {
        String data;
        data = "{\"_links\":{\"type\": {\"href\": \"http://vouper.staging.fluidobjects.com/rest/type/node/venda\"}},\n" +
                "\"title\": [{\"value\": \"Venda App\"}],\n" +
                "\"type\": [{\"target_id\": \"venda\"}],\n" +
                "\"field_anuncio\":[{\"target_id\":  2977 }],\n" +
                "\"field_comprador\":[{\"target_id\":39}]\n" +
                "}\n";
        PerfilUsuario user = new PerfilUsuario("tester","uyfgwy@ugi.ugui","ytd","123456");
        try {
            String response = JSONComunication.postAuthenticated("http://vouper.staging.fluidobjects.com/entity/node?_format=hal_json",data,user);
            assertNotEquals("-1", response);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void login() {
        String response = JSONComunication.login("tester","123456");
        assertNotEquals("-1",response);
        assertEquals("39",response);
    }
}