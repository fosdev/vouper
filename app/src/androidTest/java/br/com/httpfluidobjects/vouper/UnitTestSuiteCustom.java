package br.com.httpfluidobjects.vouper;


//import com.example.android.testing.mysample.CalculatorAddParameterizedTest;
  //      import com.example.android.testing.mysample.CalculatorInstrumentationTest;
        import org.junit.runner.RunWith;
        import org.junit.runners.Suite;

import br.com.httpfluidobjects.vouper.ExampleInstrumentedTestCustom;

// Runs all unit tests.
@RunWith(Suite.class)
@Suite.SuiteClasses({ExampleInstrumentedTestCustom.class})
public class UnitTestSuiteCustom {}